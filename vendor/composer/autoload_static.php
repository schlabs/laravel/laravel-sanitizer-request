<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit0ec72c1023c7252b77753c6cbd1b7e02
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'SchLabs\\LaravelApiExceptions\\' => 29,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'SchLabs\\LaravelApiExceptions\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit0ec72c1023c7252b77753c6cbd1b7e02::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit0ec72c1023c7252b77753c6cbd1b7e02::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit0ec72c1023c7252b77753c6cbd1b7e02::$classMap;

        }, null, ClassLoader::class);
    }
}
