<?php

/*
Filter	                Description
_____________________________________________________________________________
trim	                Trims a string
empty_string_to_null	If the given string is empty set it to null
escape	                Escapes HTML and special chars using php's filter_var
lowercase	            Converts the given string to all lowercase
uppercase	            Converts the given string to all uppercase
capitalize	            Capitalize a string
cast	                Casts a variable into the given type. Options are: integer, float, string, boolean, object, array and Laravel Collection.
format_date	            Always takes two arguments, the date's given format and the target format, following DateTime notation.
strip_tags	            Strip HTML and PHP tags using php's strip_tags
digit	                Get only digit characters from the string
 */

return [
     'name'              => ['uppercase','empty_string_to_null','cast:string'],
     'last_name'         => ['uppercase','empty_string_to_null'],
     'description'       => ['uppercase','empty_string_to_null'],
     'document_type'     => ['uppercase','empty_string_to_null','trim'],
     'document'          => ['uppercase','empty_string_to_null','trim'],
     'phone'             => ['uppercase','empty_string_to_null','trim'],
     'email'             => ['trim', 'empty_string_to_null', 'uppercase'],
     'status'            => ['cast:boolean'],
     'plate'             => ['uppercase','empty_string_to_null'],
     'events'            => ['uppercase','empty_string_to_null'],
     'methods'           => ['uppercase','empty_string_to_null'],
     'process_is_valid'  => ['cast:boolean'],
     'topic'             => ['uppercase','empty_string_to_null'],
     'number'            => ['uppercase','empty_string_to_null'],
     'brand'             => ['uppercase','empty_string_to_null'],
     'model'             => ['uppercase','empty_string_to_null'],
     'color'             => ['uppercase','empty_string_to_null'],
     'nfc_key'           => ['cast:integer'],
     'type'              => ['uppercase','empty_string_to_null'],
     'app_status'        => ['cast:boolean'],
     'guard_status'      => ['cast:boolean'],
     'gate_status'       => ['cast:boolean'],
     'notification'      => ['cast:boolean'],
];
