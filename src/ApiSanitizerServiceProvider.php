<?php

namespace SchLabs\LaravelSanitizerRequest;

use Illuminate\Support\ServiceProvider;

class ApiSanitizerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/sanitizer.php' => config_path('sanitizer.php'),
        ]);
    }

    public function register()
    {

    }
}
