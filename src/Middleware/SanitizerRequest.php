<?php
namespace App\Http\Middleware;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use JWTAuth;
use Elegant\Sanitizer\Sanitizer;

class SanitizerRequest {

    public function handle($request, \Closure  $next) {
        $request->merge(\Sanitizer::make($request->all(), config('sanitizer'))->sanitize());
        return $next($request);
    }

    /**
     * @throws \Exception
     */
    public function terminate($request, $response)
    {


    }
}
